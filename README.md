# Docker - NodeJs

![Badge](https://img.shields.io/static/v1?label=node&message=20.10.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=npm&message=10.2.5&color=blue)
![Badge](https://img.shields.io/static/v1?label=yarn&message=1.22.19&color=blue)
![Badge](https://img.shields.io/static/v1?label=nodemon&message=3.0.2&color=blue)
![Badge](https://img.shields.io/static/v1?label=pm2&message=5.3.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=oci%20instantclient&message=19.3.0&color=blue)
[![Badge](https://img.shields.io/static/v1?label=bitbucket&message=ygo_morais&color=green&logo=bitbucket)](https://bitbucket.org/ygo_morais/docker-node)
![Badge](https://img.shields.io/static/v1?label=license&message=MIT&color=green)

## Sobre

Imagem para criação de container de serviço em NodeJs.

## Criando um projeto

1 - Crie uma pasta que será o local onde ficará o projeto

```bash
mkdir <meu-projeto>
```

2 - Logo em seguide acesse a pasta criada

```bash
cd <meu-projeto>
```

3 - Por fim, crie um container para executar seu projeto.

```bash
docker container run -it --name <nome-container> --net host -v $PWD:/usr/app ygoamaral/node:latest bash
```

_Obs: Para mais segurança, especifique uma porta e remova o parâmetro '--net host'._

4 - A partir de agora, você estará em um ambiente Docker com Nodejs. Caso queira, crie seu projeto node com o comando abaixo:

```bash
npm init
```

## Retornando para o projeto

1 - Inicie o container

```bash
docker container start <nome-container>
```

2 - Acesse o projeto por esse container

```bash
docker exec -it <nome-container> bash
```

## Dica de script de execução do projeto

Crie o arquivo _.env_, que terá variáveis que poderam ser utilizadas para configurar o container do seu projeto. Logo em seguida, crie um arquivo que será responsável por iniciar o container com base nas configurações do _.env_ (Dica de nome do arquivo: _docker-service.sh_). Deixe esses arquivos na raiz do projeto:

- Exemplo de conteúdo do arquivo _.env_:

```env
PORTA=3333
SEGUNDO_PLANO=sim
DOCKER_IMAGEM=ygoamaral/node:latest
INICIAR_COM_SO=nao
DOCKER_CONTAINER=meu-servico-node
```

- Exemplo de conteúdo do arquivo _docker-service.sh_:

```bash
#!/bin/bash

# Capturando variavel de ambiente do arquivo .env
eval "$(grep ^PORTA= .env)"
eval "$(grep ^SEGUNDO_PLANO= .env)"
eval "$(grep ^DOCKER_IMAGEM= .env)"
eval "$(grep ^INICIAR_COM_SO= .env)"
eval "$(grep ^DOCKER_CONTAINER= .env)"

if [ "$SEGUNDO_PLANO" == "sim" ]; then
  PARAMETRO_SEGUNDO_PLANO=" -d "
  PARAMETRO_COMANDO=" yarn start " #adicionar comando de inicializacao no package.json na propriedade scripts.start
else
  PARAMETRO_COMANDO=" bash "
fi

PARAMETRO_INICIAR_COM_SO=$([ "$INICIAR_COM_SO" == "sim" ] && echo " --restart=always ")

excluir_container(){
  docker container rm $DOCKER_CONTAINER -f
}

iniciar_container(){
  # Criando o container
  docker container run \
  -it \
  $PARAMETRO_SEGUNDO_PLANO \
  --name $DOCKER_CONTAINER \
  $PARAMETRO_INICIAR_COM_SO \
  -p $PORTA:$PORTA \
  -v $PWD:/usr/app \
  $DOCKER_IMAGEM \
  $PARAMETRO_COMANDO
}

if [ ! "$(docker ps -aq -f name=^$DOCKER_CONTAINER$)" ]; then
  iniciar_container
else
  excluir_container && iniciar_container
fi
```

### Exemplos de utilização

- Em ambiente de desenvolvimento o arquivo _.env_ poderia ficar assim:

```env
PORTA=3333
SEGUNDO_PLANO=nao
DOCKER_IMAGEM=ygoamaral/node:latest
INICIAR_COM_SO=nao
DOCKER_CONTAINER=meu-servico-node
```

- E para o ambiente de produção, o _.env_ ficaria assim:

```env
PORTA=3333
SEGUNDO_PLANO=sim
DOCKER_IMAGEM=ygoamaral/node:latest
INICIAR_COM_SO=sim
DOCKER_CONTAINER=meu-servico-node
```

- Agora é só executar o script:

```bash
./docker-service.sh
```

_Obs1: a variável 'SEGUNDO_PLANO' pode ser usada para executar o container em background (com o valor 'sim') ou acessando o 'bash' do container (com o valor 'nao')._

_Obs2: a variável 'INICIAR_COM_SO' expecifica se o container vai iniciar junto com o sistema operacional ou não, com os respectivos valores, 'sim' ou 'nao'._
