FROM node:20.10.0

ENV LD_LIBRARY_PATH=/opt/oracle/instantclient_19_3

RUN apt update \
  && npm i -g npm \
  && npm i -g nodemon pm2 \
  && mkdir /opt/oracle \
  && apt install libaio1 -y

COPY ./instantclient-basic-linux.x64-19.3.0.0.0dbru.zip /opt/oracle

RUN cd /opt/oracle \
  && unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip \
  && rm -f instantclient-basic-linux.x64-19.3.0.0.0dbru.zip \
  && ln -sf /usr/share/zoneinfo/America/Fortaleza /etc/localtime \
  && apt-get clean -y \
  && apt-get remove -y \
  && apt-get autoclean -y \
  && apt-get autoremove -y

WORKDIR /usr/app